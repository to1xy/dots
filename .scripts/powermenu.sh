#!/bin/bash

option0="⏻ Power Off"
option1=" Reboot"
option2=" Lock"

options="$option0\n$option1\n$option2"

select="$(echo -e "$options" | rofi -dmenu -location 3 -theme powermenu)"

[[ -z select ]] && exit

case $select in
	$option0)
		shutdown now;;
	$option1)
		reboot;;
	$option2)
		slock;;
esac
